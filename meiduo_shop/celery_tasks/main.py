from celery import Celery
import os
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'meiduo_shop.settings')
# 创建Celery对象，设置celry对象的名字，可以和工程同名
app = Celery('shop')

# 告知celery 去哪里加载配置文件
app.config_from_object('celery_tasks.config')
# 指定 celery 要执行哪些任务
# app.autodiscover_tasks(列表)
app.autodiscover_tasks(['celery_tasks.sms','celery_tasks.email','celery_tasks.html'])
#设置定时任务
from celery.schedules import crontab
app.conf.beat_schedule = {
    # Executes every Monday morning at 7:30 a.m.
    'name': {
        'task': 'html',     #任务的名字
        'schedule': crontab(), #定时任务的设置
    },
}