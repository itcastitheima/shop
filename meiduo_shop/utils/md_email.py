from itsdangerous import TimedJSONWebSignatureSerializer as Serializer
# # 1. 组织数据
# sk = "abc"
# data = {
#     "id":1,
#     "email":"qi_rui_hua@163.com"
# }
# # 2. 创建序列化器
# s = Serializer(secret_key=sk,expires_in=1)
# # 3. 加密数据
# token = s.dumps(data)
# # print(token)
# # 解密
# info = s.loads("eyJhbGciOiJIUzUxMiIsImlhdCI6MTY5NDE0MTY1OCwiZXhwIjoxNjk0MTQxNjU5fQ.eyJpZCI6MSwiZW1haWwiOiJxaV9ydWlfaHVhQDE2My5jb20ifQ.vEQ9HKtoKWNJR6fQoYc3oNCFqihUaeO0wzawqWzkaj9Tkq7bs8k8QZyTwNd_MQtii831-VuQgOopfEXWxRqR0A")
# print(info)

from itsdangerous import TimedJSONWebSignatureSerializer as Serializer
from meiduo_shop import settings
def generact_active_email_url(data):
    # 1. 加密秘钥，使用 系统生成的，过期时间也放在setting
    # 2. 创建序列化器
    # s = Serializer(secret_key=加密的秘钥,expires_in=过期时间)
    s = Serializer(secret_key=settings.SECRET_KEY,
                   expires_in=settings.EMAIL_EXPIRE)
    # 3. 加密生成token  bytes类型   bytes类型.decode()  变字符串
    token = s.dumps(data).decode()
    # 4. 返回 激活连接地址
    return "http://www.meiduo.site:8080/success_verify_email.html?token="+token

def check_token(token):
    s = Serializer(secret_key=settings.SECRET_KEY,
                   expires_in=settings.EMAIL_EXPIRE)
    #数据有可能过期，或者被破坏 要异常捕获
    try:
        data = s.loads(token)
    except Exception as e:
        return None
    else:
        #没有异常走else
        return data
