"""
1. 继承自系统的Storage
2. 实现 _open 和 _save 方法
3. 要实现url方法
    实现  外链 + 图片名字的拼接
"""
from django.core.files.storage import Storage
class QiniuStorage(Storage):
    # _open 和  _save 我们只需要在类中，定义该方法就可以
    def _open(self, name, mode='rb'):
        pass
    def _save(self, name, content, max_length=None):
        pass
    def url(self, name):
        # 图片访问的地址
        # name 就是图片名字
        return "http://s3oldviem.hn-bkt.clouddn.com/" + name
