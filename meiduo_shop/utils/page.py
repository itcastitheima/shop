from collections import OrderedDict
from rest_framework.pagination import PageNumberPagination
from rest_framework.response import Response
class PageNum(PageNumberPagination):
    page_size = 3  #开启页面
    page_size_query_param = 'pagesize'

    def get_paginated_response(self, data):
        return Response(OrderedDict([
            ('count', self.page.paginator.count),
            ('page', self.page.number),
            ('pages', self.page.paginator.num_pages),
            ('pagesize', self.page_size),
            ('lists', data)
        ]))
