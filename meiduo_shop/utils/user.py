from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import JsonResponse
class MDLoginRequiredMixin(LoginRequiredMixin):
    # 当父类的某些功能 不能满足我们的需求的时候，继承 重写
    def handle_no_permission(self):
        return JsonResponse({"code":"400","errmsg":"请登录"})