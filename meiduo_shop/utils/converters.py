"""
转换器的使用步骤：
1. 定义转换器类  -- 设置正则规则
2. 注册转换器【使用前】
3. 使用
"""
from django.urls import converters
class UsernameConverter:
    regex = '[a-zA-Z0-9_]{5,12}'
    # to_python 的作用是 获取了数据之后，记得返回
    def to_python(self,value):
        return str(value)