from django.urls import path
from apps.oauth.views import QQAuthUserView
urlpatterns = [
    path('oauth_callback/', QQAuthUserView.as_view()),
]