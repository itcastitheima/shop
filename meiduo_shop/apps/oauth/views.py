from django.http import JsonResponse
from django.shortcuts import render
from django.views import View
from QQLoginTool.QQtool import OAuthQQ
from meiduo_shop import settings
from apps.oauth.models import OAuthQQUser
from django.contrib.auth import login
import json
from apps.users.models import User
# Create your views here.
class QQAuthUserView(View):
    def get(self,request):
        # 1. 获取code
        code = request.GET.get("code")
        # 2. 创建OAuthQQ对象
        qq = OAuthQQ(client_id=settings.QQ_CLIENT_ID,           #appid
                     client_secret=settings.QQ_CLIENT_SECRET,   #app secret
                     redirect_uri=settings.QQ_REDIRECT_URI)     #登录成功之后跳转的地址
        # 3. 用code换取token
        token = qq.get_access_token(code)
        # 4. 用token换取openid
        openid = qq.get_open_id(token)
        # 5. 根据 openid进行查询
        try:
            qquser = OAuthQQUser.objects.get(openid=openid)
        except Exception as e:
            # 6. 如果没有查询到 说明这个QQ号没有绑定过。要让他绑定
            # 必须返回code为 300！！！！
            return JsonResponse({"access_token":openid,"code":300,"errmsg":"OK"})
        else:
            # 7. 如果查询到 说明这个QQ好绑定过。登录  cookie和session
            # session
            login(request,qquser.user)
            # cookie
            res =  JsonResponse({"code":0,"errmsg":"OK"})
            res.set_cookie("username",qquser.user.username,24*3600)
            return res


    def post(self,request):
        # 1. 接收参数
        data = json.loads(request.body)
        # 2. 提取参数
        mobile = data.get('mobile')
        password = data.get('password')
        openid = data.get('access_token')
        # 图片验证码和短信验证码校验省略

        # 3. 根据手机号进行用户信息的查询
        try:
            user = User.objects.get(mobile=mobile)
        except Exception as e:
            # 4. 如果没有查询到，则执行注册逻辑
            user = User.objects.create_user(
                username=mobile,
                mobile=mobile,
                password=password
            )
        else:
            # 5. 如果查询到，则验证密码
            if not user.check_password(password):
                return JsonResponse({"code":400,"errmsg":"参数错误"})
        # 6. 绑定 openid和user
        OAuthQQUser.objects.create(
            user=user,
            openid=openid
        )
        # 7.session
        login(request, user)
        # 8.cookie
        res = JsonResponse({"code": 0, "errmsg": "OK"})
        res.set_cookie("username", user.username, 24 * 3600)
        return res