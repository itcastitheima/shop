from django.urls import path
from apps.orders.views import SettlementView,OrderCommitView
urlpatterns = [
    path('orders/settlement/',SettlementView.as_view() ),
    path('orders/commit/',OrderCommitView.as_view() )
]