from django.http import JsonResponse
from django.shortcuts import render
from django.views import View
from utils.user import MDLoginRequiredMixin
from apps.users.models import Address
from django_redis import get_redis_connection
from apps.goods.models import SKU
# Create your views here.
class SettlementView(MDLoginRequiredMixin,View):
    def get(self,request):
        # 0. 获取登录用户的信息
        user = request.user
        # 1. 收货地址
        addresses = Address.objects.filter(user=user,is_deleted=False)
        address_list = []
        for address in addresses:
            address_list.append({
                "id":address.id,
                'province': address.province.name,
                'city': address.city.name,
                'district': address.district.name,
                'place': address.place,
                'receiver': address.receiver,
                'mobile': address.mobile
            })
        # 2. 购物车 选中商品的信息
        #  2.1 获取redis客户端
        client = get_redis_connection('cart')
        #  2.2 获取hash 因为要获取购买数量
        sku_id_counts = client.hgetall('cart_%s'%user.id)
        #  2.3 类型转换  redis得到的数据都是bytes类型
        new_dict={}
        for sku_id,count in sku_id_counts.items():
            new_dict[int(sku_id)]=int(count)
        #  2.4 获取set  因为要获取 选中商品的id
        selected_ids = client.smembers('cart_selected_%s'%user.id)
        #  2.5 类型转换  redis得到的是bytes类型的数据
        new_selected_ids = []
        for id in selected_ids:
            new_selected_ids.append( int(id) )
        #  2.6 根据选中商品的id 获取商品信息
        skus = SKU.objects.filter(id__in=new_selected_ids)
        #  2.7 对象转字典
        sku_list=[]
        for sku in skus:
            sku_list.append({
                "id":sku.id,
                "name":sku.name,
                "count":new_dict.get(sku.id),
                "default_image_url":sku.default_image.url,
                "price":sku.price
            })
        # 3. 运费   价格最好使用  价格类型 float,double ,decimal[货币类型]
        # 100 分期  3期   33.33   33.33    33.34
        from decimal import Decimal
        freight = Decimal(10)

        context = {
            'addresses': address_list,
            'skus': sku_list,
            'freight': freight,
        }
        return JsonResponse({"code":0,"errmsg":"OK","context":context})
"""
提交订单：
1.  为了减少 前端传递的数据量 分析前度必传的参数是 地址id和支付方式
2.  先保存订单基本信息，再保存订单商品信息。因为 订单商品信息里需要 订单基本信息的外键
3.  保存订单基本信息的时候，先把订单总金额计为0.  在保存订单商品的时候，对总价格进行统计
    这样可以减少 对商品的遍历
"""
import json
from apps.orders.models import OrderInfo,OrderGoods
from datetime import datetime
from decimal import Decimal
class OrderCommitView(MDLoginRequiredMixin,View):
    def post(self,request):
        # 1. 接收参数
        data = json.loads(request.body)
        # 2. 提取参数
        address_id = data.get('address_id')
        pay_method = data.get('pay_method')
        # 3. 验证参数
        # 3.1 判断是这个用户的地址
        try:
            address = Address.objects.get(id=address_id,user=request.user)
        except Exception as e:
            return JsonResponse({"code":400,"errmsg":"地址信息不对"})
        # 3.2 判断支付方式
        # pay_method 只能满足1 或者2
        # if pay_method not in [1,2]:
        if pay_method not in [OrderInfo.PAY_METHODS_ENUM["CASH"],OrderInfo.PAY_METHODS_ENUM["ALIPAY"]]: #增强了代码的可读性
            return JsonResponse({"code":400,"errmsg":"支付信息不对"})
        status = 0
        if pay_method == OrderInfo.PAY_METHODS_ENUM['CASH']:
            status = OrderInfo.ORDER_STATUS_ENUM['UNSEND']  #现金支付，状态是未发货
        else:
            status = OrderInfo.ORDER_STATUS_ENUM['UNPAID']  #支付宝，状态 待支付
        # 4. 数据入库
        # 4.1 保存订单基本信息
        # order_id = yyyymmddhhmmss + 9位用户id
        # str from time  把时间转化为字符串   精确到毫秒  微秒  纳秒
        # 1 2 00000003    %d 整数   09 表示 不满9位 用0占位
        order_id = datetime.now().strftime('%Y%m%d%H%M%S%f') + '%09d'%request.user.id
        # with open():   自动关闭文件句柄
        from django.db import transaction
        with transaction.atomic():
            # 创建一个事务开始点  开始事务
            start_point = transaction.savepoint()
            try:
                orderinfo = OrderInfo.objects.create(
                    order_id = order_id,
                    user=request.user,      #对象=对象，  整数=整数
                    # user_id=request.user.id,
                    # user_id=request.user  #错误写法
                    address_id=address.id,
                    total_count=0,
                    total_amount=Decimal('0'),
                    freight=Decimal('10.0'),
                    pay_method=pay_method,
                    status=status
                )
                # 4.2 保存订单商品信息
                client = get_redis_connection('cart')
                # hash -- {sku_id:count}
                sku_id_counts = client.hgetall('cart_%s'%request.user.id)
                new_dict={}
                for sku_id,count in sku_id_counts.items():
                    new_dict[int(sku_id)] = int(count)
                # set -- {}  选中商品的id
                selected_ids = client.smembers('cart_selected_%s'%request.user.id)
                new_list=[]
                for id in selected_ids:
                    new_list.append(int(id))
                # 对选中商品的id 进行遍历， 遍历过程中，将购买的信息保存到 数据库
                # 库存减少，销量增加
                # 累加计算 订单基本信息中的数量和金额
                for id in new_list:
                    sku = SKU.objects.get(id=id)
                    # 先判断库存是否大于购买量
                    sale_count = new_dict.get(id)
                    if sku.stock < sale_count:  #库存不充足，直接返回响应
                        transaction.savepoint_rollback(start_point)
                        return JsonResponse({"code":400,"errmsg":"库存不足"})
                    import time
                    time.sleep(10)  #让我们的进程 睡10秒钟
                    # 库存减少，销量增加
                    # sku.stock = sku.stock - sale_count
                    # sku.sales = sku.sales + sale_count
                    # sku.save()  #更新数据一定要记得保存
                    # 乐观锁代码
                    # ① 需要之前的库存记录
                    old_stock = sku.stock
                    # ② 先查询之后的库存记录，然后再更新数据
                    new_stock = old_stock - sale_count
                    new_sales = sku.sales + sale_count
                    result = SKU.objects.filter(id=id,stock=old_stock).update(stock=new_stock,
                                                                     sales=new_sales)
                    # result 是 更新数据 受影响的行数
                    if result:  # 0 转换为布尔值是假  1,2,3，.. 转换为布尔是真
                        print("成功")
                    else:
                        print("失败")
                        transaction.savepoint_rollback(start_point)
                        return JsonResponse({"code":400,"errmsg":"下单失败"})
                    OrderGoods.objects.create(
                        order=orderinfo,        #订单基本信息 外键
                        sku = sku,              #sku
                        count = sale_count,     #售卖数量
                        price = sku.price       #售卖单价
                    )

                    # 累加计算订单基本信息中的数量和金额
                    orderinfo.total_count += sale_count
                    orderinfo.total_amount += (sale_count * sku.price)
                #循环结束之后，最后再更新 订单的基本信息
                # 4.3 更新订单基本信息的总金额和总数量
                orderinfo.save()
            except Exception as e:
                #日志记录  事务回滚
                transaction.savepoint_rollback(start_point)
            else:
                #事务提交  -- 可以不提交， with 可以自动提交
                transaction.savepoint_commit(start_point)

        # 下单成功之后，应该将购物车中，选中的相关信息 删除
        # client.srem('cart_selected_%s'%request.user.id,*new_list)
        # client.hdel('cart_%s'%request.user.id,*new_list)
        # 5. 返回响应
        return JsonResponse({"code":0,"errmsg":"OK","order_id":orderinfo.order_id})