from django.shortcuts import render
from django.views import View
from utils.goods import get_categories
from apps.contents.models import ContentCategory
# Create your views here.
class IndexView(View):
    def get(self,request):
        # 1. 获取分类数据
        categories = get_categories()
        # 2. 获取广告和1f，2f等数据
        contents = {}
        content_categories = ContentCategory.objects.all()
        for cat in content_categories:
            contents[cat.key] = cat.content_set.filter(status=True).order_by('sequence')
        # 3. 进行HTML页面渲染
        # response = render(请求对象，HTML模板,渲染的数据)
        # 渲染的数据 key必须 按照课件来
        context = {
            'categories': categories,
            'contents': contents,
        }
        response = render(request,'index.html',context)
        # 4. 返回响应
        return response

from apps.goods.models import SKU
from django.http import JsonResponse
class ListView(View):
    def get(self,request,category_id):
        ordering = request.GET.get('ordering')
        # 先根据分类把所有数据查询出来
        skus = SKU.objects.filter(category_id=category_id).order_by(ordering)

        from django.core.paginator import Paginator
        # 字典变量名.get(key字符串,默认值)
        per_page = request.GET.get('page_size',5) #每页多少条记录
        page = request.GET.get('page',1)  #获取第几页
        #创建分页对象
        # p = Paginator(数据列表,每页多少条记录)
        p = Paginator(skus,per_page)
        # 获取指定页码的数据
        page_skus = p.page(page)
        # skus 是对象列表 需要进行 对象转换字典
        data_list = []
        for sku in page_skus:
            data_list.append({ "id":sku.id,
                'default_image_url': sku.default_image.url,
                'name': sku.name,
                'price': sku.price})
        from apps.goods.models import GoodsCategory
        import logging      #导入日志
        #获取面包屑数据
        # ① 根据分类id 获取分类对象
        try:
            category=GoodsCategory.objects.get(id=category_id)
        except Exception as e:
            logger = logging.getLogger('django')    #获取日志器
            logger.error(e)                         #记录日志
            breadcrumb = {}
        else:
            # ② 调用 封装好的面包屑方法
            from utils.goods import get_breadcrumb
            breadcrumb = get_breadcrumb(category)

        data = {"code":0,"errmsg":"OK",
                "list":data_list,
                "count":p.num_pages,
                "breadcrumb":breadcrumb}
        return JsonResponse(data)

class HotView(View):
    def get(self,request,category_id):
        # 1. 根据 分类id查询数据，并对数据进行排序，获取前2条数据
        skus = SKU.objects.filter(category_id=category_id).order_by('-sales')[0:2]
        # 2. 对象列表转换为字典列表
        data_list = []
        for sku in skus:
            data_list.append({"id": sku.id,
                              'default_image_url': sku.default_image.url,
                              'name': sku.name,
                              'price': sku.price})
        # 3. 返回响应
        return JsonResponse({"code":0,"errmsg":"OK","hot_skus":data_list})
import logging
from utils.goods import get_breadcrumb,get_goods_specs
class DetailView(View):
    def get(self,request,sku_id):
        # 1. 根据id查询sku的具体信息
        try:
            sku = SKU.objects.get(id=sku_id)
        except Exception as e:
            logger = logging.getLogger("django")
            logger.error(e)
            return JsonResponse({"code":400,"errmsg":"没有此商品"})
        # 2. 分类数据
        categories = get_categories()
        # 3. 面包屑数据
        breadcrumb = get_breadcrumb(sku.category)
        # 4. 规格信息
        goods_specs = get_goods_specs(sku)
        context = {
            'categories': categories,
            'breadcrumb': breadcrumb,
            'sku': sku,
            'specs': goods_specs,
        }
        # 首页是通过 render 渲染
        # 详情页面也是 通过 render 渲染   8000
        return render(request,'detail.html',context)
from apps.goods.models import GoodsVisitCount
from datetime import date
class VisitView(View):
    def post(self,request,category_id):
        today = date.today()
        # 1. 先要查询 当天的这个分类的数据 有没有
        try:
            gvc = GoodsVisitCount.objects.get(category_id=category_id,
                                              date=today)
        except Exception as e:
            # 没有查询到
            # 2. 如果没有，则需要 新增
            GoodsVisitCount.objects.create(category_id=category_id,
                                              date=today,
                                                count=1)
        else:
            # 3. 如果有，则需要 更新操作
            gvc.count += 1      # count = count + 1
            gvc.save()
        return JsonResponse({"code":0,"errmsg":"OK"})

"""
redis
    string      
    hash        key:value   没有顺序
    list        列表         有顺序[会重复]    v      [id,id,id,id]
    set         集合         没有顺序
    zset        有序集合      有顺序           v

"""