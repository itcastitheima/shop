from django.shortcuts import render

# Create your views here.
"""
1.  有些网站的购物车 需要登录   有些网站的购物车 不需要登录   明确产品的需求
    登录用户添加购物车  我们的美多
    
2.  购物车的功能  ----  增删改查

3.  数据库 字段的设计   id, user_id, sku_id, count, selected

4. 从学习 和 真是的企业开发 -- redis

依据： ① 以实现功能为标准    ② 内存是有限的，节省资源      

抽象问题具体化
user_id, sku_id, count, selected
   1       666     6        true
   1       777     7        false
   1       888     8        true
   2       666     10       true
   2       999      1       true
    key:  value         hash        方案①
    user_id:      sku_id:count,  sku_id_statue:selected
       1            666:6           666_statue: true
                    777:7           777_statue: false
     key: value       hash + set    方案②
     user_id:      sku_id:count         set
       1            666:6               666
                    777:7                    
    key: value       hash           方案③
     user_id:      sku_id:count        
       1            666:6              
                    777:-7           
                    888:8    
redis
    string      字符串
    hash        字典
    list        列表
    set         集合
    zset        有序集合
"""
from django.views import View
from utils.user import MDLoginRequiredMixin
import json
from apps.goods.models import SKU
from django.http import JsonResponse
from django_redis import get_redis_connection
class CartView(MDLoginRequiredMixin,View):
    def post(self,request): #新增购物车
        # 1. 接收参数
        data = json.loads(request.body)
        # 2. 校验参数 -- 查询商品是否存在
        sku_id = data.get("sku_id")
        try:
            sku = SKU.objects.get(id=sku_id)
        except Exception as e:
            return JsonResponse({"code":400,"errmsg":"没有此商品"})
        # 3. 数据入库--redis
        # 3.1 创建redis客户端
        client = get_redis_connection('cart')
        # 3.2 hash      sku_id:count
        # client.hset(key,field,value)
        # client.hset('cart_%s'%request.user.id,sku_id,1)
        client.hincrby('cart_%s'%request.user.id,sku_id,1)
        # 3.2 set       选中的商品id
        client.sadd('cart_selected_%s'%request.user.id,sku_id)
        # 4. 返回响应
        return JsonResponse({"code":0,"errmsg":"OK"})

    def get(self,request):
        # 1. 根据用户信息，查询redis
        user_id = request.user.id
        client = get_redis_connection('cart')
        # 2. hash,set
        #  获取hash所有数据   {sku_id:count,sku_id:count}
        sku_id_counts = client.hgetall('cart_%s'%user_id)
        # 获取set数据
        selected_ids = client.smembers('cart_selected_%s'%user_id)
        # 类型转换   字典转换
        new_dict = {}
        # for item in sku_id_counts.items():
        for key,value in sku_id_counts.items():
            new_dict[int(key)]=int(value)
        # 类型转换   集合
        new_list = []
        for item in selected_ids:
            new_list.append(int(item))
        # 3. 根据id 查询商品的详细信息  只获取字典的 key呢？？
        # sku_id_counts.keys()  #[ 1,2,3,4]
        # select * from xxx where id in (1,2,3,4)
        skus = SKU.objects.filter(id__in=new_dict.keys())
        # 4. 对象转换为字典
        data_list = []
        for sku in skus:
            data_list.append({
                "id":sku.id,
                "name":sku.name,
                "default_image_url":sku.default_image.url,
                "price":sku.price,
                "count":  new_dict.get(sku.id),      #购买数量
                "selected":  sku.id in new_list,     # in 在不在
                "amount":  sku.price * new_dict.get(sku.id)#总价格  单价 * 数量
            })
        # 5. 返回响应
        return JsonResponse({"code":0,"errmsg":"OK","cart_skus":data_list})

    def put(self,request):
        # 修改是采用的一个接口地址   每次提交的时候 都把修改的数据提交上来
        # 修改数量 有2种方式 ① +1 或者 -1   ② 最终的数量  12  9
        # 1. 接收数据
        data = json.loads( request.body )
        # 2. 提取数据
        sku_id = data.get('sku_id')
        count = data.get('count')
        selected = data.get('selected')
        # 3. 校验数据
        try:
            sku = SKU.objects.get(id=sku_id)
        except Exception as e:
            return JsonResponse({"code":400,"errmsg":"商品不存在"})
        # count 能够转换为整数
        try:
            count = int(count)
        except Exception as e:
            return JsonResponse({"code":400,"errmsg":"商品数量不对"})
        # if not isinstance(selected,bool):
        #     return JsonResponse({"code": 400, "errmsg": "选中状态不对"})
        # 4. 更新数据
        client = get_redis_connection('cart')
        # hash   hset(key,field,value)  如果有key，则更新数据
        client.hset('cart_%s'%request.user.id , sku_id , count)  # count
        # set
        if selected:    #选中 selected 有值
            client.sadd('cart_selected_%s'%request.user.id,sku_id)
        else:
            client.srem('cart_selected_%s'%request.user.id,sku_id)
        # 5. 返回响应
        cart_sku = {
            'id': sku_id,
            'count': count,
            'selected': selected,
            'name': sku.name,
            'default_image_url': sku.default_image.url,
            'price': sku.price,
            'amount': sku.price * count,
        }
        # 必须把修改之后的数量，选中状态 给前端。 前后端数据一致！！！
        return JsonResponse({"code":0,"errmsg":"OK","cart_sku":cart_sku})

    def delete(self,request):
        # 1. 接收参数
        data = json.loads(request.body)
        # 2. 提取参数
        sku_id = data.get("sku_id")
        # 3. 验证参数
        try:
            sku = SKU.objects.get(id=sku_id)
        except Exception as e:
            return JsonResponse({"code":400,"errmsg":"没有此商品"})
        # 4. 删除数据
        client = get_redis_connection('cart')
        # ① 创建管道
        pl = client.pipeline()
        #  hash
        # hdel
        # ② 管道收集命令
        pl.hdel('cart_%s'%request.user.id,sku_id)
        #  set
        pl.srem('cart_selected_%s'%request.user.id,sku_id)
        # ③ 执行管道
        pl.execute()
        # 5. 返回响应
        return JsonResponse({"code":0,"errmsg":"OK"})

class CartSelectionView(MDLoginRequiredMixin,View):
    def put(self,request):
        """
        实现全选或者全不选
        """
        # 1. 接收参数 post,put,delete   body
        # 通过json.loads() 将JSON数据转换为Python的字典数据
        data = json.loads(request.body)
        # 2. 提取参数
        selected = data.get('selected')
        # 3. 判断 是全选 还是 全不选
        client = get_redis_connection('cart')
        # 获取购物车中商品信息
        sku_id_counts = client.hgetall('cart_%s' % request.user.id)
        # 再获取所有id   字典.keys()
        ids = sku_id_counts.keys()
        if selected:
            # 4. 全选 把购物车商品id 全放到集合里
            # [1,2,3]
            # 添加到选中集合里
            # 方式① 遍历
            # for id in ids:
            #     client.sadd('cart_selected_%s'%request.user.id,id)
            # 方式②
            # *列表 就是 拆包      **字典 对字典的拆包
            # client.sadd('cart_selected_%s'%request.user.id,*[1,2,3])
            # client.sadd('cart_selected_%s'%request.user.id,1,2,3)
            client.sadd('cart_selected_%s'%request.user.id,*ids)
        else:
            # 5. 全不选 把集合里的数据 都删除
            # 方式① 把key删除了
            # client.delete('cart_selected_%s'%request.user.id)
            client.srem('cart_selected_%s'%request.user.id,*ids)
        # 6. 返回响应
        return JsonResponse({"code":0,"errmsg":"OK"})

def demo(*args,**kwargs):
    pass
# args 元组   kwargs 字典
