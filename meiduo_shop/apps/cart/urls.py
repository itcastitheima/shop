from django.urls import path
from apps.cart.views import CartView,CartSelectionView
urlpatterns = [
    path('carts/',CartView.as_view() ),
    path('carts/selection/',CartSelectionView.as_view() ),
]