
# 省的数据 不需要 上一级id
# 市区县 只要给我一个 上级id，我对应的查询下一级数据就行
# /areas/
# /areas/<pk>/
from django.urls import path
from apps.areas.views import ProvinceView,SubsView
urlpatterns = [
    path('areas/',ProvinceView.as_view() ), #省
    path('areas/<pk>/',SubsView.as_view() ), #市区县
]