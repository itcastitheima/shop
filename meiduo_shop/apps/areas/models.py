from django.db import models
"""
省           id      上一级id
河南省     10000       None
河北省     20000       None
安徽省     30000       None

市
郑州市     10010       10000
新乡市     10020
安阳市     10030
洛阳市     10040
三门峡市   10050

区县
高新区     10011        10010
金水区     10012
二七区     10013
港区       10014

"""
class Area(models.Model):
    """省市区"""
    # id
    name = models.CharField(max_length=20, verbose_name='名称')
    parent = models.ForeignKey('self', on_delete=models.SET_NULL, related_name='subs', null=True, blank=True, verbose_name='上级行政区划')
    # subs 下一级
    class Meta:
        db_table = 'tb_areas'
        verbose_name = '省市区'
        verbose_name_plural = '省市区'

    def __str__(self):
        return self.name