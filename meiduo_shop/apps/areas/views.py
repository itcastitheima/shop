from django.shortcuts import render
from django.views import View
from apps.areas.models import Area
from django.http import JsonResponse
# Create your views here.
class ProvinceView(View):
    def get(self,request):
        # 默认保存在redis的0号库
        from django.core.cache import cache
        # 获取redis的数据
        pro_list = cache.get('pro_list')
        # 判断有没有数据
        if not pro_list:  # [] 空数据 转换为布尔 就是False
            # 1. 根据模型获取查询结果集
            # SELECT * FROM tb_areas
            # WHERE parent_id is NULL;
            data = Area.objects.filter(parent__isnull=True)
            # 2. 对查询结果集进行遍历转换
            pro_list = []
            for item in data:
                pro_list.append({"id": item.id,"name": item.name})
            # 缓存设置方法
            # cache.set(key,value,seconds)
            cache.set('pro_list',pro_list,30*24*3600)
        # 3. 返回响应
        return JsonResponse({"code":0,'errmsg':"ok","province_list":pro_list})
class SubsView(View):
    def get(self,request,pk):
        from django.core.cache import cache
        # ① 从redis中，获取缓存
        res_data = cache.get("subs:%s"%pk)
        # ② 判断有没有缓存数据
        if not res_data:
            # SELECT * FROM tb_areas
            # WHERE parent_id = 410000;
            # 0. 根据 pk 先查询当前 级别的数据
            try:
                data = Area.objects.get(id=pk)
            except Exception as e:
                return JsonResponse({'code':400,'errmsg':"查询错误"})
            # 1. 根据条件 查询 下一级数据
            subs = data.subs.all()
            # 2. 将对象列表转换为字典列表
            subs_list = []
            for item in subs:
                subs_list.append({
                    "id":item.id,
                    "name":item.name
                })
            res_data = {"id":data.id,"name":data.name,"subs":subs_list}
            # ③ 如果没有缓存数据，则把查询之后的字典数据保存
            # cache.set(key,value,seconds)
            cache.set('subs:%s'%pk,res_data,30*24*3600)
        # 3. 返回响应
        return JsonResponse({'code':0,"errmsg":"OK","sub_data":res_data})