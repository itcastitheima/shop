from django.db import models

# Create your models here.
# Django自带的用户名模型类
from django.contrib.auth.models import AbstractUser

class User(AbstractUser):
    # create table xxx  ( mobile varchar(11) unique key, xxx)
    # 字段名=models.类型(选项)
    #① 父类不能满足子类需求的时候，可以借助于 继承！
    #② 父类的方法也可以继承，父类有对密码加密的方法
    #添加一个手机号
    mobile = models.CharField(max_length=11,  #Charfiled 必须设置max_length
                              unique=True)  # 选项 唯一键
    email_active = models.BooleanField(default=False)
    default_address = models.ForeignKey('Address', related_name='users', null=True, blank=True,
                                        on_delete=models.SET_NULL, verbose_name='默认地址')
    #orderinfo_set
    # 修改一下默认的表明
    class Meta:
        db_table = 'tb_users'


from utils.models import BaseModel
class Address(BaseModel):
    """用户地址"""
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='addresses', verbose_name='用户')
    title = models.CharField(max_length=20, verbose_name='地址名称')
    receiver = models.CharField(max_length=20, verbose_name='收货人')
    province = models.ForeignKey('areas.Area', on_delete=models.PROTECT, related_name='province_addresses', verbose_name='省')
    city = models.ForeignKey('areas.Area', on_delete=models.PROTECT, related_name='city_addresses', verbose_name='市')
    district = models.ForeignKey('areas.Area', on_delete=models.PROTECT, related_name='district_addresses', verbose_name='区')
    place = models.CharField(max_length=50, verbose_name='地址')
    mobile = models.CharField(max_length=11, verbose_name='手机')
    tel = models.CharField(max_length=20, null=True, blank=True, default='', verbose_name='固定电话')
    email = models.CharField(max_length=30, null=True, blank=True, default='', verbose_name='电子邮箱')
    is_deleted = models.BooleanField(default=False, verbose_name='逻辑删除')

    class Meta:
        db_table = 'tb_address'
        verbose_name = '用户地址'
        verbose_name_plural = verbose_name
        ordering = ['-update_time']