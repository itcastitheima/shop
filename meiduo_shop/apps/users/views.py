from django.shortcuts import render
from django.views import View
from apps.users.models import User
from django.http import JsonResponse
class UserNameCountView(View):
    def get(self,request,name):
        # 1. 接收参数  路径参数
        # 2. 验证参数【可以跳过前端发送请求，要保证数据的正确 验证】
        # 3. 调用模型，查询用户名的个数
        count = User.objects.filter(username=name).count()
        # 4. 返回响应
        return JsonResponse({'code':0,
                             'errmsg':'OK',
                             'count':count})
class MobileCountView(View):
    def get(self,request,mobile):
        # 1. 接收参数  路径参数
        # 2. 验证参数【可以跳过前端发送请求，要保证数据的正确 验证】
        # 3. 调用模型，查询用户名的个数
        count = User.objects.filter(mobile=mobile).count()
        # 4. 返回响应
        return JsonResponse({'code':0,
                             'errmsg':'OK',
                             'count':count})
import json
from apps.users.models import User
class RegisterView(View):
    def post(self,request):
        # 1. 接收参数 -- request.body  bytes类型的数据
        # bytes --> 字典
        data=json.loads(request.body)
        # 2. 校验参数 【省略】
        # 3. 数据入库
        # User.objects.create() 没有加密 密码
        user=User.objects.create_user(username=data.get('username'),
                                      password=data.get('password'),
                                      mobile=data.get('mobile'))

        # 添加状态保持
        from django.contrib.auth import login
        # login(请求对象，登录的用户对象)
        login(request,user)
        # 4. 返回响应
        return JsonResponse({"code":0,"errmsg":"OK"})

from libs.captcha.captcha import captcha
# from libs.包名.模块名 import 对象名
from django_redis import get_redis_connection
from django.http import HttpResponse
class ImageView(View):
    def get(self,request,uuid):
        # 3. 生成图片验证码
        # code 是图片验证码的字母内容，例如： xyza
        # image 是图片二进制
        image_code,image =captcha.generate_captcha()
        print(image_code)
        # 4. 把图片验证码的值 保存到redis
        # 安装了一个  django-redis
        redis_client = get_redis_connection("code")
        # redis_client.setex(key,second,value)
        redis_client.setex(uuid,300,image_code)
        # 5. 返回响应，告知 浏览器我们返回的数据类型
        # return HttpResponse(内容,content_type=类型字符串)
        # 类型: 大类/小类     image/jpeg      image/png     application/json
        return HttpResponse(image,content_type='image/jpeg')

class CodeView(View):
    def get(self,request,mobile):
        #/sms_codes/18310820005/?image_code=aaaa&image_code_id=c528cbcd-fe15-479a-8f51-94f864e6be36
        # 1. 接收数据 -- mobile 手机号 路径   uuid和用户填写的验证码 查询参数
        image_code = request.GET.get('image_code')
        image_code_id = request.GET.get('image_code_id')
        # 2. 验证数据
        if not all([image_code_id,image_code]):
            return JsonResponse({"code":400,"errmsg":"缺少参数"})
        # 3. 比对图片验证码
        # 3.1 获取redis的验证码
        redis_cli = get_redis_connection('code')
        redis_image = redis_cli.get(image_code_id)
        # 3.2 删除redis里的验证码
        redis_cli.delete(image_code_id)
        # 3.3 比对
        if redis_image.decode().lower() != image_code.lower():
            return JsonResponse({"code":400,"errmsg":"图片验证码错误"})
        # 4. 生成短信验证码
        # 4.1 生成短信验证码
        from random import randint
        code = randint(1000,9999)
        import logging
        logger = logging.getLogger("django")
        logger.info(code)
        # 4.2 保存
        redis_cli.setex(mobile,300,code)

        # 5. 发送短信验证码
        # from libs.rlyun.send_sms_code import send_code
        # send_code(mobile,code)
        from celery_tasks.sms.tasks import send_code
        # 注意： 必须使用 异步任务名.delay(参数)   参数同任务名参数
        send_code.delay(mobile,code)
        # 6. 返回响应
        return JsonResponse({"code":0,"errmsg":"成功"})
from django.contrib.auth import login,authenticate
import re
class LoginView(View):
    def post(self,request):
        # 将bytes类型的数据转换为字典数据
        data = json.loads(request.body)
        # 1. 接收参数
        username = data.get('username')
        password = data.get('password')
        # 2. 验证参数【省略】
        # from django.db.models import Q
        # User.objects.filter(  Q(mobile=username) | Q(username=username)  )
        # User.USERNAME_FIELD 这个属性 可以设置 哪个字段 作为用户名
        if re.match('1[3-9]\d{9}',username):
            print("手机号")
            # user = User.objects.get(mobile=username)
            User.USERNAME_FIELD = 'mobile'
        else:
            print("用户名")
            # user = User.objects.get(username=username)
            User.USERNAME_FIELD = 'username'

        # 3. 根据用户登录信息 查询user
        try:
            # user = User.objects.get(username=username)
            user = authenticate(username=username,password=password)
        except User.DoesNotExist:   #用户不存在异常
            return JsonResponse({'code':400,'errmsg':'用户名或密码错误'})
        # 单独校验密码
        # if not user.check_password(password):
        #     return JsonResponse({'code': 400, 'errmsg': '用户名或密码错误'})
        # 4. 状态保持
        login(request,user)
        # 5. 返回响应
        response = JsonResponse({"code":0,"errmsg":"OK"})
        response.set_cookie('username',user.username,7*24*3600)
        return response
from django.contrib.auth import logout
class LogoutView(View):
    def delete(self,request):
        # 清楚session数据
        logout(request)
        # 清除 cookie数据
        response = JsonResponse({"code":0,"errmsg":"OK"})
        response.delete_cookie('username')
        return response
# Mixin 继承的最前边
from django.contrib.auth.mixins import LoginRequiredMixin
from utils.user import MDLoginRequiredMixin
# class CenterView(LoginRequiredMixin,View):
class CenterView(MDLoginRequiredMixin,View):
    def get(self,request):
        # request.user 是系统 通过中间件添加的。登录用户是user 未登录用户是 匿名用户
        info_data = {
            "username":request.user.username,
            "mobile":request.user.mobile,
            "email":request.user.email,
            "email_active":request.user.email_active
        }
        return JsonResponse({"code":"0","errmsg":"OK","info_data":info_data})

class EmailView(MDLoginRequiredMixin,View):
    def put(self,request):
        # 1. 获取参数   post,put 请求体
        data = json.loads(request.body)
        email = data.get("email")
        # 2. 验证参数 [省略]
        # 3. 更新数据
        request.user.email = email
        request.user.save() #记得保存！！！
        # 4. 发送激活邮件
        from utils.md_email import generact_active_email_url
        verify_url = generact_active_email_url({"id":request.user.id,"email":email})

        from celery_tasks.email.tasks import send_active_email
        send_active_email(verify_url=verify_url,
                                email=email)
        # 5. 返回响应
        return JsonResponse({'code':0,'errmsg':'OK'})
from utils.md_email import check_token
class EmailVerifiView(View):
    def put(self,request):
        # 1. 获取 加密的令牌
        token = request.GET.get('token')
        # 2. 对加密的令牌 进行解密
        data = check_token(token)
        # 3. 根据解密的数据 查询用户信息
        if data is None:
            return JsonResponse({'code':400,"errmsg":'数据过期或者数据错误'})
        id = data.get('id')
        email = data.get('email')
        try:
            user = User.objects.get(id=id,email=email)
        except Exception as e:
            return JsonResponse({'code': 400, "errmsg": '数据过期或者数据错误'})
        # 4. 修改用户的邮箱状态
        user.email_active=True
        user.save()
        # 5. 返回响应
        return JsonResponse({'code': 0, "errmsg": 'OK'})
from utils.user import MDLoginRequiredMixin
from apps.users.models import Address
class AddressView(MDLoginRequiredMixin,View):
    def post(self,request):
        # 查询用户的地址数量 是多少
        count = Address.objects.filter(user=request.user).count()
        if count >= 20:
            return JsonResponse({"code":400,"errmsg":"收货地址数量 达到上限"})
        # 不是表单数据，是JSON数据
        json_dict = json.loads(request.body)
        # 1. 接收参数
        receiver = json_dict.get("receiver") #收货人
        province_id = json_dict.get('province_id')
        city_id = json_dict.get('city_id')
        district_id = json_dict.get('district_id')
        place = json_dict.get('place')
        mobile = json_dict.get('mobile')
        tel = json_dict.get('tel')
        email = json_dict.get('email')
        # 2. 验证参数 【省略】
        # 3. 数据入库
        address = Address.objects.create(user=request.user,
                title = receiver,
                receiver = receiver,
                province_id = province_id,
                city_id = city_id,
                district_id = district_id,
                place = place,
                mobile = mobile,
                tel = tel,
                email = email)
        # address = Address(receiver=receiver,
        #                        user=request.user)
        # address.save()
        # 设置默认地址
        if not request.user.default_address:
            request.user.default_address = address
            #更新数据必须要 调用模型的save方法
            request.user.save()

        address_dict = {
            "id": address.id,
            "title": address.title,
            "receiver": address.receiver,
            "province": address.province.name,
            "city": address.city.name,
            "district": address.district.name,
            "place": address.place,
            "mobile": address.mobile,
            "tel": address.tel,
            "email": address.email
        }
        # 4. 返回响应
        return JsonResponse({"code":0,"errmsg":"Ok","address":address_dict})

class AddressListView(MDLoginRequiredMixin,View):
    def get(self,request):
        # 1. 根据 用户信息，查询地址结果集
        addresses = Address.objects.filter(user=request.user,
                                           is_deleted=False)
        # 2. 将查询结果集转换为字典列表。遍历
        list_data = []
        for address in addresses:
            list_data.append({
                "id": address.id,
                "title": address.title,
                "receiver": address.receiver,
                "province": address.province.name,
                "city": address.city.name,
                "district": address.district.name,
                "place": address.place,
                "mobile": address.mobile,
                "tel": address.tel,
                "email": address.email
            })
        # request.user 是登录用户
        # user.default_address 是默认地址对象
        default_address_id = request.user.default_address.id
        # 3. 返回响应
        return JsonResponse({"code":0,
                             "errmsg":"OK",
                             "addresses":list_data,
                             "default_address_id":default_address_id})

class AddressUpdateView(MDLoginRequiredMixin,View):
    def put(self,request,id):
        # 1. 接收数据   request.GET   request.POST   request.body
        json_dict = json.loads(request.body)
        # 2. 验证数据[省略]
        receiver = json_dict.get('receiver')
        province_id = json_dict.get('province_id')
        city_id = json_dict.get('city_id')
        district_id = json_dict.get('district_id')
        place = json_dict.get('place')
        mobile = json_dict.get('mobile')
        tel = json_dict.get('tel')
        email = json_dict.get('email')
        # 3. 更新数据
        Address.objects.filter(id=id).update(
            user=request.user,
            title=receiver,
            receiver=receiver,
            province_id=province_id,
            city_id=city_id,
            district_id=district_id,
            place=place,
            mobile=mobile,
            tel=tel,
            email=email
        )

        # address = Address.objects.get()
        # address.receiver=recevier
        # address.save()

        address = Address.objects.get(id=id)
        address_dict = {
            "id": address.id,
            "title": address.title,
            "receiver": address.receiver,
            "province": address.province.name,
            "city": address.city.name,
            "district": address.district.name,
            "place": address.place,
            "mobile": address.mobile,
            "tel": address.tel,
            "email": address.email
        }
        # 4. 返回响应
        return JsonResponse({"code":0,'errmsg':'ok','address':address_dict})

    def delete(self,request,id):
        # 1. 根据id 查询数据
        try:
            address = Address.objects.get(id=id)
        except Exception as e:
            return JsonResponse({"code":400,'errmsg':'没有此地址'})
        else:
            # 2. 物理删除或者逻辑删除
            # 没有异常 走这里
            address.is_deleted=True
            address.save()
        # 3. 返回响应
        return JsonResponse({'code':0,'errmsg':'ok'})

class AddressDeaultView(MDLoginRequiredMixin,View):
    def put(self,request,id):
        # 1. 根据id值 查询地址
        try:
            # 查询条件越精确，bug越少 --  漏洞
            address = Address.objects.get(id=id,user=request.user)
        except Exception as e:
            return JsonResponse({'code':400,'errmsg':'没有此地址'})
        # 2. 修改数据
        # 对象 = 对象
        request.user.default_address = address
        # id 数值 = id数值
        # request.user.default_address.id = address.id
        request.user.save()
        # 3. 返回响应
        return JsonResponse({'code':0,'errmsg':'OK'})

class AddressTitleView(MDLoginRequiredMixin,View):
    def put(self,request,id):
        # 1. 根据id查询 地址信息
        try:
            address = Address.objects.get(id=id)
        except Exception as e:
            return JsonResponse({'code':400,'errmsg':'没有此地址'})
        # 2. 接收参数
        title = json.loads(request.body).get('title')
        # 3. 更新数据
        address.title=title
        address.save()
        # 4. 返回响应
        return JsonResponse({'code':0,'errmsg':'ok'})

class ChangePWDView(MDLoginRequiredMixin,View):
    def put(self,request):
        # 1. 接收参数
        data = json.loads(request.body)
        old_pwd = data.get('old_password')
        new_pwd = data.get('new_password')
        new_pwd2 = data.get('new_password2')
        # 2. 验证参数【省略】
        # 3. 验证当前密码是否正确
        if not request.user.check_password(old_pwd):
            return JsonResponse({'code':400,'errmsg':'原密码错误'})
        # 4. 修改密码
        request.user.set_password(new_pwd)
        request.user.save()
        # 5. 清除原来登录的信息
        logout(request)
        # 6. 返回响应
        response = JsonResponse({'code':0,'errmsg':'ok'})
        response.delete_cookie('username')
        return response
from apps.goods.models import SKU
from django_redis import get_redis_connection
####浏览历史记录
class HistoryView(MDLoginRequiredMixin,View):
    def post(self,request):  #新增浏览记录
        # 1. 接收参数
        data = json.loads(request.body)
        # 2. 提取参数和验证参数
        sku_id = data.get('sku_id')
        try:
            sku = SKU.objects.get(id=sku_id)
        except Exception as e:
            return JsonResponse({"code":400,"errmsg":"没有此商品"})
        # 3. 存放到 redis列表
        # 3.1  创建redis客户端
        client = get_redis_connection('history')
        # 3.2  先删除列表中 可能存在的数据
        # client.lrem(key,count,value)
        client.lrem('history_%s'%request.user.id,0,sku_id)
        # 3.3  再从列表的左边添加进去
        client.lpush('history_%s'%request.user.id,sku_id)
        # 4. 返回响应
        return JsonResponse({"code":0,"errmsg":"OK"})

    def get(self,request):
        # 1. 获取redis的浏览记录
        # 1.1 创建redis客户端
        client = get_redis_connection('history')
        # 1.2 获取数据   lrange(key,start,stop)
        redis_list = client.lrange('history_%s'%request.user.id,0,4)
        # [1,2,3]   select * from xxx where id in []
        # 2.遍历数据，根据商品id，查询商品详细信息
        data_list = []
        for sku_id in redis_list:
            sku = SKU.objects.get(id=sku_id)
            # # 3. 将对象转换成字典数据
            data_list.append( {
                "id":sku.id,
                "name":sku.name,
                "price":sku.price,
                "default_image_url":sku.default_image.url
            } )
        # 4. 返回响应
        return JsonResponse({"code":0,"errmsg":"OK","skus":data_list})
        #作业，保证列表中 只保留最多5条记录 怎么实现