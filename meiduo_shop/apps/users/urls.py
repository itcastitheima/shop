from django.urls import path
from apps.users.views import UserNameCountView,RegisterView,MobileCountView
from apps.users.views import ImageView,CodeView
from apps.users.views import LoginView,LogoutView
from apps.users.views import CenterView,EmailView,EmailVerifiView,AddressView
from apps.users.views import AddressListView,AddressUpdateView,AddressDeaultView
from apps.users.views import AddressTitleView,ChangePWDView,HistoryView
urlpatterns = [
    path('usernames/<username:name>/count/',UserNameCountView.as_view()),
    path('mobiles/<mobile>/count/',MobileCountView.as_view()),
    path('register/',RegisterView.as_view()),
    path('image_codes/<uuid>/',ImageView.as_view()),
    path('sms_codes/<mobile>/',CodeView.as_view()),
    path('login/',LoginView.as_view()),
    path('logout/',LogoutView.as_view()),
    path('info/',CenterView.as_view()),
    path('emails/',EmailView.as_view()),
    path('emails/verification/',EmailVerifiView.as_view()),
    path('addresses/create/',AddressView.as_view()),
    path('addresses/',AddressListView.as_view()),
    path('addresses/<id>/',AddressUpdateView.as_view()),
    path('addresses/<id>/default/',AddressDeaultView.as_view()),
    path('addresses/<id>/title/',AddressTitleView.as_view()),
    path('password/',ChangePWDView.as_view()),
    # 浏览记录
    path('browse_histories/',HistoryView.as_view()),
]