from django.urls import path
from apps.payment.views import AlipayView,SaveAlipayView
urlpatterns = [
    path('payment/status/',SaveAlipayView.as_view() ),  # 必须在前边
    path('payment/<order_id>/',AlipayView.as_view() ),

]