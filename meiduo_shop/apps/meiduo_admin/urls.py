from django.urls import path
from rest_framework_jwt.views import obtain_jwt_token
from apps.meiduo_admin.views.loginview import MISObtainJSONWebToken
from apps.meiduo_admin.views import statisticalview
from apps.meiduo_admin.views import userview
from apps.meiduo_admin.views import image,sku,sysman
urlpatterns = [
    # 2个思路。 一个就是 自己看源码  另外一个就是看文档
    # path('authorizations/',  obtain_jwt_token ),
    path('authorizations/',  MISObtainJSONWebToken.as_view() ),
    path('statistical/total_count/',statisticalview.TotalUserCountAPIView.as_view() ),
    path('statistical/day_increment/',statisticalview.DailyAddUserAPIView.as_view() ),
    path('statistical/day_active/',statisticalview.DailyActiveUserAPIView.as_view() ),
    path('statistical/day_orders/',statisticalview.DailyOrderUserAPIView.as_view() ),
    path('statistical/month_increment/',statisticalview.MonthUserAPIView.as_view() ),
    #############user
    path('users/',userview.UserListCreateAPIView.as_view() ), #三级视图
    # path('users/',userview.UserGenericMixinAPIView.as_view() ), #二级视图+mixin
    # path('users/',userview.UserGenericAPIView.as_view() ), # 纯二级视图

    #获取所有的sku
    path('skus/simple/', image.AllSKUListAPIView.as_view() ),

    #获取三级分类
    path('skus/categories/', sku.ThreeListAPIView.as_view() ),

    #获取spu
    path('goods/simple/', sku.SPUListAPIView.as_view() ),


    path('goods/<pk>/specs/', sku.SPUSpecsListAPIView.as_view() ),

    #权限 类型
    path('permission/content_types/', sysman.ContentTypeListAPIView.as_view() ),

    #获取所有权限
    path('permission/simple/', sysman.SimplePermissionListAPIView.as_view() ),
    #获取所有分组
    path('permission/groups/simple/', sysman.AllGroupListAPIView.as_view() ),

]

from rest_framework.routers import DefaultRouter
from apps.meiduo_admin.views.image import SKUImageModelViewSet
#1.创建路由对象
router = DefaultRouter()
#2.设置【注册】路由规则    books/ 列表视图  books/id/详情视图   公共部分
router.register('skus/images',SKUImageModelViewSet)
#3.把生成的url追加到 urlpatterns
urlpatterns += router.urls

from apps.meiduo_admin.views.sku import SKUModelViewSet
#2.设置【注册】路由规则    books/ 列表视图  books/id/详情视图   公共部分
router.register('skus',SKUModelViewSet,basename='skus')
#3.把生成的url追加到 urlpatterns
urlpatterns += router.urls

from apps.meiduo_admin.views.sysman import PermissionModelViewSet
#2.设置【注册】路由规则    books/ 列表视图  books/id/详情视图   公共部分
router.register('permission/perms',PermissionModelViewSet,basename='perms')
#3.把生成的url追加到 urlpatterns
urlpatterns += router.urls

from apps.meiduo_admin.views.sysman import GroupModelViewSet
#2.设置【注册】路由规则    books/ 列表视图  books/id/详情视图   公共部分
router.register('permission/groups',GroupModelViewSet,basename='group')
#3.把生成的url追加到 urlpatterns
urlpatterns += router.urls


from apps.meiduo_admin.views.sysman import StaffModelViewSet
#2.设置【注册】路由规则    books/ 列表视图  books/id/详情视图   公共部分
router.register('permission/admins',StaffModelViewSet,basename='user')
#3.把生成的url追加到 urlpatterns
urlpatterns += router.urls



