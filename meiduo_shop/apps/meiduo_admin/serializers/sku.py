from rest_framework import serializers
from apps.goods.models import SKU,SKUSpecification
from django.db import transaction
class SKUSpecificationModelSerializer(serializers.ModelSerializer):
    spec_id = serializers.IntegerField()
    option_id = serializers.IntegerField()
    class Meta:
        model = SKUSpecification
        fields = ['spec_id','option_id']
class SKUSerializer(serializers.ModelSerializer):
    # spu_id 和 category_id 应用写入 write
    spu_id = serializers.IntegerField()
    category_id = serializers.IntegerField()
    # spu和category应用响应  read
    spu = serializers.StringRelatedField()
    category = serializers.StringRelatedField()

    specs = SKUSpecificationModelSerializer(many=True)
    class Meta:
        model = SKU
        fields = '__all__'

    def create(self, validated_data):

        with transaction.atomic():
            save_point = transaction.savepoint()
            try:
                # 1. 先把字典中 多的数据 移除。 用一个变量接收 多的数据
                specs = validated_data.pop('specs')
                # 2. 剩下的就是 1对多中的 1的数据。保存1的数据
                sku = SKU.objects.create(**validated_data)
                # 3. 循环遍历保存 多的数据
                for spec in specs:
                    #       多的数据里要设置 1的外键
                    SKUSpecification.objects.create(sku=sku,**spec)
                    #添加测试
                    # raise Exception("人为测试异常")
            except Exception as e:
                #有异常回滚
                transaction.savepoint_rollback(save_point)
                # sku = None
            else:
                #没有异常 提交。 在with语句下，不提交也没有关系。with会自动提交
                transaction.savepoint_commit(save_point)
            # 最后 返回 1的对象
            return sku

    def update(self, instance, validated_data):
        # 1. 获取 1对多中 ，多的数据【validated_data 多的数据 删除】
        specs = validated_data.pop('specs')
        # 2. 更新1的数据
        # User.objects.filter().update(**validated_data)
        # user = User.object.get()
        # user.name=xxx
        # user.save()
        # validated_data 就是和 sku有关系
        # 调用父类，让父类 去更新数据
        super().update(instance, validated_data)

        # for attr, value in validated_data.items():
        #         setattr(instance, attr, value)
        # instance.save()
        # 3. 更新多的数据
        for spec in specs:
            # spec = {"spec_id":xxx,"option_id":xxx}
            # 查询的时候 一定要根据 sku和规格来查询
            SKUSpecification.objects.filter(sku=instance,
                                            spec_id=spec.get("spec_id")).update(**spec)

        return instance


from apps.goods.models import GoodsCategory
class ThreeGoodsCategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = GoodsCategory
        fields = ['id','name']

from apps.goods.models import SPU
class SPUSerializer(serializers.ModelSerializer):
    class Meta:
        model = SPU
        fields = ['id','name']

# 多   规格选项
from apps.goods.models import SpecificationOption
class SPUSpecificationModelSerializer(serializers.ModelSerializer):
    class Meta:
        model = SpecificationOption
        fields = ['id','value']
# 1   规格
from apps.goods.models import SPUSpecification
class SPUSpecModelSerializer(serializers.ModelSerializer):
    #需要自己写 一对多的 序列化器嵌套
    # 多个 要添加 many=True
    options = SPUSpecificationModelSerializer(many=True)
    class Meta:
        model = SPUSpecification
        fields = '__all__'