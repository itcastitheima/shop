from apps.users.models import User
from rest_framework import serializers

class UserModelSerializer(serializers.ModelSerializer):
    # password = serializers.CharField(write_only=True)
    class Meta:
        model = User #设置模型类
        fields =  ['id','username','mobile','email','password']  #设置字段
        extra_kwargs = {
            "password": {"write_only":True}
            # "字段名": {"选项key":value}
        }
    #不要写在 class Meta里边 和 class Meta同级
    def create(self, validated_data):
        #让父类 先完成他的工作
        user = super().create(validated_data)
        # user = User.objects.create(**validated_data)  #可以
        user.set_password(user.password)  #然后 我们再对密码进行加密
        # user.set_password(validated_data.get("password"))
        user.save()
        return user