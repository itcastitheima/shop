from apps.goods.models import SKUImage
from rest_framework import serializers
# 因为有模型，所有使用 ModelSerializer
class SKUImageModelSerializer(serializers.ModelSerializer):
    class Meta:
        model = SKUImage
        fields = '__all__'

###############获取所有sku的序列化器
from apps.goods.models import SKU
class SKUModelSerializer(serializers.ModelSerializer):
    class Meta:
        model = SKU
        fields = ('id','name')