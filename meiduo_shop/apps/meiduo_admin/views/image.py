
from apps.goods.models import SKUImage
from apps.meiduo_admin.serializers.image import SKUImageModelSerializer
from rest_framework.viewsets import ModelViewSet
class SKUImageModelViewSet(ModelViewSet):
    queryset =  SKUImage.objects.all()          #查询结果集
    serializer_class = SKUImageModelSerializer  #序列化

    def create(self, request, *args, **kwargs):
        # 1. 接收参数
        # ① sku_id
        sku_id = request.POST.get("sku")
        # ② image
        image = request.FILES.get("image")
        # 4. 上传图片到七牛云
        from utils.qiuniu_upload import qiniu_upload
        # image.read() 读取二进制流
        result = qiniu_upload(image.read())
        image_path = result.get('key')
        # 5. 数据入库
        img = SKUImage.objects.create(
            sku_id = sku_id,   # 对象=对象，id=id
            image=image_path)
        from rest_framework.response import Response
        return Response({ 'id': img.id,'sku': sku_id, 'image': img.image.url},status=201)

    def update(self, request, *args, **kwargs):
        pass


##########################获取所有sku的视图
from rest_framework.generics import ListAPIView
from apps.goods.models import SKU
from apps.meiduo_admin.serializers.image import SKUModelSerializer
class AllSKUListAPIView(ListAPIView):
    queryset = SKU.objects.all()
    serializer_class = SKUModelSerializer
    #视图可以设置 单独的分页类，它的优先级很高
    #在这里设置了分页类，就不再使用 settings.py
    pagination_class = None
