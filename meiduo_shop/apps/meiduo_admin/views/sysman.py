"""
1. 用户模型
2. 角色模型【组】
3. 权限模型
"""
# 用户
from apps.users.models import User
# 角色【组】
from django.contrib.auth.models import Group
# 权限
from django.contrib.auth.models import Permission
from django.contrib.auth.models import ContentType

from rest_framework.viewsets import ModelViewSet
from apps.meiduo_admin.serializers.sysman import PermissionSerializer
class PermissionModelViewSet(ModelViewSet):
    queryset = Permission.objects.all()
    serializer_class = PermissionSerializer

from rest_framework.generics import ListAPIView
from django.contrib.auth.models import ContentType
from apps.meiduo_admin.serializers.sysman import ContentTypeSerializer
class ContentTypeListAPIView(ListAPIView):
    queryset = ContentType.objects.all()
    serializer_class = ContentTypeSerializer
    pagination_class = None

from django.contrib.auth.models import Group
from apps.meiduo_admin.serializers.sysman import GroupSerializer
class GroupModelViewSet(ModelViewSet):
    queryset = Group.objects.all()
    serializer_class = GroupSerializer

class SimplePermissionListAPIView(ListAPIView):
    queryset = Permission.objects.all()
    serializer_class = PermissionSerializer
    pagination_class = None

from apps.users.models import User
from apps.meiduo_admin.serializers.sysman import StaffUserSerializer
class StaffModelViewSet(ModelViewSet):
    queryset = User.objects.filter(is_staff=True)
    serializer_class = StaffUserSerializer

class AllGroupListAPIView(ListAPIView):
    queryset = Group.objects.all()
    serializer_class = GroupSerializer
    pagination_class = None