from apps.users.models import User
from rest_framework.generics import ListCreateAPIView
from apps.meiduo_admin.serializers.userserializer import UserModelSerializer
class UserListCreateAPIView(ListCreateAPIView):
    #查询结果集
    # queryset = User.objects.filter(username__contains=)
    # queryset = User.objects.all()
    # 我们要根据逻辑返回查询结果集，用方法
    def get_queryset(self):
        # 怎么获取查询参数 keyword???  加断点 分析
        keyword = self.request.query_params.get("keyword")
        if keyword:
            return User.objects.filter(username__contains=keyword)
        else:
            return User.objects.all()


    # 序列化器
    serializer_class = UserModelSerializer

from rest_framework.views import APIView
from rest_framework.generics import GenericAPIView
"""
APIView             drf的基类
GenericAPIView      一般和mixin配合使用非常简单   只要有分页，就要先选择 GenericAPIView以以上
ListAPIView         连请求方法【get,post。。。】都不用写
"""
# 获取所有用户信息的一个思路
# 1. 获取所有查询结果集    instances = User.object.all()
# 2. 使用序列化器 将对象列表转换为 字典列表 s = Serializer(instances)
# 3. 返回响应   Response(s.data)
from rest_framework.mixins import ListModelMixin
class UserGenericMixinAPIView(ListModelMixin,GenericAPIView):
    queryset = User.objects.all()
    serializer_class = UserModelSerializer
    def get(self,request):
        return self.list(request)

from rest_framework.response import Response
from utils.page import PageNum
class UserGenericAPIView(GenericAPIView):
    queryset = User.objects.all()
    serializer_class = UserModelSerializer
    pagination_class = PageNum
    def get(self,request):
        # 1. 获取所有查询结果集    instances = User.object.all()
        # instances = self.queryset     #属性
        instances = self.get_queryset() #方法
        # 2. 使用序列化器 将对象列表转换为 字典列表 s = Serializer(instances)
        # s = self.serializer_class(instances,many=True)
        s = self.get_serializer(instances,many=True)
        # 3. 返回响应   Response(s.data)
        return Response(s.data)

