from rest_framework.viewsets import ModelViewSet
from apps.meiduo_admin.serializers.sku import SKUSerializer
from apps.goods.models import SKU
from rest_framework.permissions import IsAuthenticated,IsAdminUser,AllowAny,IsAuthenticatedOrReadOnly
from rest_framework.permissions import DjangoModelPermissions
class SKUModelViewSet(ModelViewSet):
    # queryset = SKU.objects.all()
    # 对模型进行 增删改查的权限管理
    # 只是对 SKU视图集起作用
    permission_classes = [DjangoModelPermissions]

    def get_queryset(self):
        keyword = self.request.query_params.get("keyword")
        if keyword:
            return SKU.objects.filter(name__contains=keyword)
        else:
            return SKU.objects.all()
    serializer_class = SKUSerializer
from rest_framework.generics import ListAPIView
from apps.goods.models import GoodsCategory
from apps.meiduo_admin.serializers.sku import ThreeGoodsCategorySerializer
class ThreeListAPIView(ListAPIView):
    queryset = GoodsCategory.objects.filter(subs=None)
    serializer_class = ThreeGoodsCategorySerializer
    pagination_class = None

from apps.goods.models import SPU
from apps.meiduo_admin.serializers.sku import SPUSerializer
class SPUListAPIView(ListAPIView):
    queryset = SPU.objects.all()
    serializer_class = SPUSerializer
    pagination_class = None

from apps.goods.models import SPUSpecification
from apps.meiduo_admin.serializers.sku import SPUSpecModelSerializer
class SPUSpecsListAPIView(ListAPIView):
    # queryset = SPU.objects.all()
    def get_queryset(self):
        pk = self.kwargs.get("pk")
        return SPUSpecification.objects.filter(spu_id=pk)
    serializer_class = SPUSpecModelSerializer
    pagination_class = None