from rest_framework.views import APIView
from apps.users.models import User
from rest_framework.response import Response
from datetime import date
class TotalUserCountAPIView(APIView):
    def get(self,request):
        # 查询语句 要结合具体的需求
        count = User.objects.all().count()
        # count = User.objects.filter(is_active=True,is_staff=False,is_superuser=False).count()
        return Response({"count":count,"date":date.today()})

class DailyAddUserAPIView(APIView):
    def get(self,request):
        # today 2023-11-01 00:00:00
        count = User.objects.filter(date_joined__gt=date.today()).count()
        return Response({"count":count,"date":date.today()})
class DailyActiveUserAPIView(APIView):
    def get(self,request):
        # today 2023-11-01 00:00:00
        count = User.objects.filter(last_login__gt=date.today()).count()
        return Response({"count":count,"date":date.today()})

class DailyOrderUserAPIView(APIView):
    def get(self,request):
        # today 2023-11-01 00:00:00
        count = User.objects.filter(orderinfo__create_time__gte=date.today()).distinct().count()
        return Response({"count":count,"date":date.today()})
"""
# 1. 获取当天的日期
# 2. 进行循环 我们要获取30天的 数据
#     大于某一个时间  小于某一个时间
# 3. 查询某一天的日增用户
# 4. 追加到一个列表就可以
"""
from datetime import timedelta
class MonthUserAPIView(APIView):
    def get(self,request):
        # 1. 获取当天的日期
        today = date.today()
        # 2. 进行循环 我们要获取30天的 数据
        data_list = []
        for i in range(30):
            #     大于某一个时间  小于某一个时间
            #  2023-11-1 00:00:00 ~ 2023-11-2 00:00:00
            end = today - timedelta(days=i)
            start = today - timedelta(days=i+1)
            # 3. 查询某一天的日增用户
            count = User.objects.filter(date_joined__gte=start,
                                        date_joined__lt=end).count()
            # 4. 追加到一个列表就可以
            data_list.append( {
                "count":count,
                "date":start
            } )
            # data_list.insert(-1,{})
        #列表反转
        data_list.reverse()
        return Response(data_list)