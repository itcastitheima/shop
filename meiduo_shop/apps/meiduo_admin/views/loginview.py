from rest_framework_jwt.views import JSONWebTokenAPIView
from apps.meiduo_admin.serializers.loginserializer import LoginSerializer

class MISObtainJSONWebToken(JSONWebTokenAPIView):
    """
    API View that receives a POST with a user's username and password.

    Returns a JSON Web Token that can be used for authenticated requests.
    """
    serializer_class = LoginSerializer